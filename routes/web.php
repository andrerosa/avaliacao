<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'EventsController@index');
Route::get('/event/{event}/details', 'EventsController@details')->name('event.detail');
Route::get('/event/{event}/ticket/checkout', 'CheckoutsController@index')->name('ticket.checkout');
Route::post('/event/{event}/ticket/checkout', 'CheckoutsController@store')->name('ticket.store');
