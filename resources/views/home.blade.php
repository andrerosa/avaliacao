@extends('layouts.app')

@section('content')

<div class="container">
	<div class="section">
		
		<h2>Lista de Eventos</h2>

		<div class="row">
			@foreach($events as $event)
	        
	        <div class="col s12 m4">
		      	<div class="card">
		        	<div class="card-image">
		          		<img src="{{ $event->thumb}}">
		          		<span class="card-title">{{ $event->description }}</span>
		          		<a href="{{ route('event.detail', $event->id) }}" class="btn-floating halfway-fab waves-effect btn-large waves-light pink darken-3 buy-ticket">
		          			<i class="material-icons">keyboard_arrow_right</i>
		          		</a>
		        	</div>
		        	<div class="card-content">
		          		<p class="pink-text darken-3">{{ $event->city }} / {{ $event->state }}</p>
	          			<span class="pink darken-3 new badge" data-badge-caption=""> 
	          			@if($event->ticket->quantity > 0)
	          				<b>R$ {{ number_format($event->ticket->price, 2, ',', '') }} </b>
	          			@else
	          				<b>ESGOTADO</b>
	          			@endif
	          			</span>
		          		<p>{{ $event->address }}</p>
		          		<p>{{ $event->date->format('d/m/Y') }} às {{ $event->date->format('h:m') }}h</p>
		        	</div>
		      	</div>
		    </div>

	        @endforeach
	    </div>

	</div>
</div>

@endsection