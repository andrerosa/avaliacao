@extends('layouts.app')

@section('content')

<div class="section">
	<div class="container">

		<div class="row">
	  		<a href="/" id="download-button" class="btn-large waves-effect waves-light pink darken-3">
	  			<i class="material-icons left">keyboard_arrow_left</i>
	  			HOME
	  		</a>
		</div>

		<div class="row">
			<div class="card green lighten-5">
		    	<div class="card-content">
		    		<h5 class="center"><i class="material-icons medium">done</i></h5>
		    		<h2 class="center">Seu ingresso será processado!</h2>
		    	</div>
		    </div>
		</div>

	</div>
</div>

@endsection