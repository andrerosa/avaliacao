<!DOCTYPE html>
<html>
    <head>
        <title>Exercício | Lista de Eventos</title>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="shortcut icon" href="https://cdn2.iconfinder.com/data/icons/font-awesome/1792/ticket-16.png">
        <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css" media="screen,projection"/>
        <link rel="stylesheet" type="text/css" href="{{ url('assets/css/app.css') }}">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>
    <body>
        @yield('content')
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script type="text/javascript" src="{{ url('assets/js/app.js') }}"></script>
    </body>
</html>
