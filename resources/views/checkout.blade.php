@extends('layouts.app')

@section('content')

<div class="section">
	<div class="container">

		<div class="row">
	  		<a href="{{ route('event.detail', $event->id) }}" id="download-button" class="btn-large waves-effect waves-light pink darken-3">
	  			<i class="material-icons left">keyboard_arrow_left</i>
	  			Voltar
	  		</a>
		</div>

		<div class="col s12">

    		<h2 class="header">
    			<i class="material-icons medium">local_play</i>
    			{{ $event->description }}
    		</h2>

    		<p><b>{{ $event->date->format('d/m/Y') }} - {{ $event->state }} / {{ $event->city }}</b></p>

    		@if ($errors->any())
			    <div class="card pink lighten-5">
			    	<div class="card-content">
			    		<i class="material-icons">error</i> <b>Esquecemos algo no caminho!</b> <br/>
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li><b>* {{ $error }}</b></li>
				            @endforeach
				        </ul>
				    </div>
			    </div>
			@endif

    		<div class="card grey lighten-5">
    			<div class="card-content">
    				<form method="post" action="{{ route('ticket.store', $event->id) }}" name="ticketForm" class="container">
    					@csrf
    					<div class="row">
		      				<div class="input-field col s6">
						    	<select name="quantity" id="quantity">
						      		<option value="0" disabled selected>Escolha uma quantidade</option>
						      		@for ($i = 1; $i <= $event->ticket->buy_limit; $i++)
						      			<option value="{{ $i }}">{{ $i }}</option>
						      		@endfor
						    	</select>
						    	<label>Quantidade de Ingressos</label>
						  	</div>
		      				<div class="input-field col s6">
						    	<select name="half" id="half" data-price="{{ $event->ticket->price }}" data-half="{{ $event->ticket->halfEntrance }}">
						      		<option value="0">INTEIRA</option>
						      		<option value="1">MEIA ENTRADA</option>
						    	</select>
						    	<label>Tipo de ingresso</label>
						  	</div>
						</div>
						<div class="row">
							<div class="input-field col s6">
					          	<input name="user_name" id="portador" type="text" class="validate">
					          	<label for="portador">Nome do Portador</label>
					        </div>
					        <div class="input-field col s6">
					          	<input disabled value="" id="ticket-value" type="text">
					        </div>
						</div>
	      			</form>
	      			<br/>
      				<a href="javascript: document.ticketForm.submit()" class="btn-large pink darken-3 right buy-ticket">
    					<i class="material-icons left">local_play</i>
      					Efetuar compra
      				</a>
    			</div>
    		</div>

  		</div>

	</div>
</div>

@endsection