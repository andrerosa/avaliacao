@extends('layouts.app')

@section('content')

<div id="index-banner" class="parallax-container">
    <div class="section no-pad-bot">
      	<div class="container">
        	<h1 class="header center white-text">{{ $event->description }}</h1>
        	<div class="row center">
          		<h5 class="header col s12 white-text">Dia {{ $event->date->format('d/m/Y') }} às {{ $event->date->format('h:m') }}h</h5>
        	</div>
        	<div class="row left">
          		<a href="/" id="download-button" class="btn-large waves-effect waves-light pink darken-3">
          			<i class="material-icons left">keyboard_arrow_left</i>
          			Home
          		</a>
        	</div>
        	@if($event->ticket->quantity > 0)
	        	<div class="row right">
	          		<a href="{{ route('ticket.checkout', $event->id) }}" class="btn-large waves-effect waves-light pink darken-3">
	          			Comprar ingresso
	          			<i class="material-icons right">keyboard_arrow_right</i>
	          		</a>
	        	</div>
        	@endif
      	</div>
    </div>
    <div class="parallax">
    	<img src="{{ $event->thumb }}">
    </div>
</div>

<div class="container">
	<div class="row card-detail">
	    <div class="col s12">
	      	<div class="card">
	        	<div class="card-content">

	        		<span class="card-title">
	          			<h4>{{ $event->description }}</h4>
	          		</span>

	        		<div class="container">
		        		<div class="row">
		        			<div class="col s12 m4">
		        				<div class="icon-block">
	            					<h2 class="center pink-text text-darken-3">
	            						<i class="material-icons medium">place</i>
	            					</h2>
	            					<h5 class="center">{{ $event->city }} / {{ $event->state }}</h5>
	            					<p class="center">{{ $event->address }}</p>
	          					</div>
		        			</div>
		        			<div class="col s12 m4">
		        				<div class="icon-block">
	            					<h2 class="center pink-text text-darken-3">
	            						<i class="material-icons medium">timer</i>
	            					</h2>
	            					<h5 class="center">
	            						Dia {{ $event->date->format('d/m/Y') }} às {{ $event->date->format('h:m') }}h
	            					</h5>
	          					</div>
		        			</div>
		        			@isset($event->ticket->price)
		        			<div class="col s12 m4">
		        				<div class="icon-block">
	            					<h2 class="center pink-text text-darken-3">
	            						<i class="material-icons medium">local_play</i>
	            					</h2>
	            					<h5 class="center">R$ {{ number_format($event->ticket->price, 2, ',', '') }}</h5>
	            					@if($event->ticket->quantity > 0)
	            						<p class="center">Restam <b>{{ $event->ticket->quantity }}</b> ingressos!</p>
	            					@else
				          				<p class="center"><b>Ingressos esgotados!</b></p>
				          			@endif
	          					</div>
		        			</div>
	          				@endisset
		        		</div>
		        	</div>

		        	<div class="divider"></div>
		        	<br/>

		        	<p class="center">{{ $event->details }}</p>

	        	</div>
	      	</div>
	    </div>
	</div>
</div>

@endsection