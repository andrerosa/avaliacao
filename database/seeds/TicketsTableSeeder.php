<?php

use Illuminate\Database\Seeder;
use App\Avaliacao\Ticket;

class TicketsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tickets')->delete();
        $json = File::get("database/data/tickets.json");
        $data = json_decode($json);

        foreach ($data as $obj) {
            Ticket::create(array(
                'event_id' => $obj->event_id,
                'quantity' => $obj->quantity,
                'price' => $obj->price,
                'buy_limit' => $obj->buy_limit,
            ));
        }
    }
}
