<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use App\Avaliacao\Event; 

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('events')->delete();
        $json = File::get("database/data/events.json");
        $data = json_decode($json);

        foreach ($data as $obj) {
            Event::create(array(
                'description' => $obj->description,
                'state' => $obj->state,
                'city' => $obj->city,
                'address' => $obj->address,
                'date' => $obj->date,
                'details' => $obj->details,
                'active' => $obj->active,
                'thumb' => $obj->thumb
            ));
        }
    }
}
