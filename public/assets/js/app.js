$(document).ready(function(){
    $('.parallax').parallax();
    $('select').formSelect();

    let total = 0.00;

    $('#half').on('change', function(){
    	ticketAmount();
    });

    $('#quantity').on('change', function(){
    	ticketAmount();
    });

    const ticketAmount = function() {
		let quantity = $("#quantity").val();

		let half = $("#half").data('half');
		let price = $("#half").data('price');

		if ($("#half").val() == "1") {
			total = (quantity * half); // Half Entrance
		} else {
			total = (quantity * price); // Intire
		}

		$('#ticket-value').val("R$ "+total.toFixed(2).replace(".",","));
	}
});