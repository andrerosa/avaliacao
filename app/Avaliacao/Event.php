<?php

namespace App\Avaliacao;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
	protected $dates = ['date'];
	protected $fillable = [
		'description', 'state', 'city', 'address', 'date', 'details', 'thumb', 'active'
	];

    public function ticket()
    {
    	return $this->hasOne('App\Avaliacao\Ticket');
    }

    public function scopeActives($query)
    {
    	return $query->where('active', true)->get();
    }
}
