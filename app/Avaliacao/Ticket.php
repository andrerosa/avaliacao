<?php

namespace App\Avaliacao;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
	protected $fillable = [
		'event_id', 'quantity', 'price'
	];

	protected $appends = ['halfEntrance'];

	public function event()
	{
		return $this->belongsTo('App\Avaliacao\Event');
	}

	public function getHalfEntranceAttribute()
    {
    	return ($this->price / 2);
    }
}
