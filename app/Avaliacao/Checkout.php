<?php

namespace App\Avaliacao;

use Illuminate\Database\Eloquent\Model;
use App\Avaliacao\Event;

class Checkout extends Model
{
   protected $fillable = [
   		'ticket_id', 
   		'quantity',
   		'amount',
   		'user_name',
   		'half'
   ];
}
