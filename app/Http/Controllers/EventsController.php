<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Avaliacao\Event;

class EventsController extends Controller
{

	public function index() 
	{
		$events = Event::actives();
		return view('home', ['events' => $events]);
	}

	public function details($idevent)
	{
		$event = Event::findOrFail($idevent);
		return view('detail', ['event' => $event]);
	}

}
