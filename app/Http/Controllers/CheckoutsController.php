<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTicket;
use App\Avaliacao\Event;
use App\Avaliacao\Checkout;

class CheckoutsController extends Controller
{
	public function index($eventId)
	{
		$event = Event::findOrFail($eventId);
		return view('checkout', ['event' => $event]);
	}

    public function store($eventId, StoreTicket $request)
    {
    	$event = Event::find($eventId);

    	$data['quantity'] 	= $request->post('quantity');
    	$data['half'] 		= (int) $request->post('half');
    	$data['user_name'] 	= $request->post('user_name');
    	$data['ticket_id'] 	= $event->ticket->id;

    	$value = (!! $data['half']) 
                ? $event->ticket->halfEntrance 
				: $event->ticket->price;

    	$data['amount'] = $data['quantity'] * $value;

    	Checkout::create($data);

    	return view('finish');
    }
}
