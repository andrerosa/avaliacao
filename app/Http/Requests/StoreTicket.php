<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreTicket extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'quantity' => 'required|integer',
            'user_name' => 'required|string',
            'half' => 'required|string'
        ];
    }

    public function messages()
    {
        return [
            'quantity.required' => 'O campo quantidade é necessário!',
            'user_name.required' => 'Precisamos saber seu nome!',
            'half.required' => 'Escolha o tipo de ingresso!'
        ];
    }
}
